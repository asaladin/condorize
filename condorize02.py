#!/usr/bin/python2.4
# condorize version 0.1
# (C)2007 Adrien Saladin
# adrien.saladin@ibpc.fr



def isPathAbsolute(path):
    if path[0]=='/': return True



class structure:
    pass





import sys
import os
from subprocess import Popen,PIPE
import string

if (len(sys.argv) < 2):
	print """
	USAGE: condorize command_to_send_to_cluster
	"""


#from optparse import OptionParser
#parser = OptionParser()
#parser.add_option("-o", dest="outputdir",default='.', type="string", help="creates outputdirectory")
#(options, args) = parser.parse_args()

options=structure()


if sys.argv[1][0]=='-':
    print "je detecte une option"
    if sys.argv[1][1]=='o':
        #the option is an output dir directive
        options.outputdir=sys.argv[2]
        args=sys.argv[3:]
    else:
        #unknown option
        print "unrecognized option"
        raise SystemExit
else:
    #first parameter is a command
    options.outputdir='.'
    args=sys.argv[1:]





#print warning:
print """**************************************************************
Running Condorize version 0.2
03/20/2007
**************************************************************

WARNING: if your program creates new files, files will be created in the current directory.
Therefore you may not run multiple instances of the same program in the same directory.
Sorry for this but it is a Condor limitation... not my fault!
"""




exe=args[0]
arguments=args[1:]



#get working directory
p = Popen("pwd", shell=True,stdout=PIPE,stderr=PIPE)
WorkingDir = string.join(p.stdout.readlines() )
outerr = string.join(p.stderr.readlines() )



#get full command path
p = Popen("which %s"%(exe) , shell=True,stdout=PIPE,stderr=PIPE)
cmd_fullpath =  string.join(p.stdout.readlines() )


#check if output already exists:
if (not os.path.exists(options.outputdir) ):
    print "Creating directory: %s"%options.outputdir
    os.makedirs(options.outputdir)
else:
    print "Output directory already exists... Checking if output files may be overwritten..."
    if (os.path.exists(options.outputdir+"/out") or \
        os.path.exists(options.outputdir+"/log") or  \
        os.path.exists(options.outputdir+"/error")):
            print "Error: one of the output file already exists... \nAborting"
            raise SystemExit
    print "ALL CLEAR!\n"




#check if outputdir is relative to the current directory or absolute path:
if(isPathAbsolute(options.outputdir)):
    #print "you gave an absolute path for the output dir (this is ok)"
    outdir=options.outputdir
else:
    #the path is relative to the current directory
    #print "the output path is relative to the current directory (it's ok)"
    outdir="M4_WD/" + options.outputdir


#get list of all local files
p = Popen("ls -p -1" , shell=True,stdout=PIPE,stderr=PIPE)
localFileList =  p.stdout.readlines()
cleanFilelist=[]
for s in localFileList:
        cleanstring=s.rstrip("\r\n")
	if cleanstring[-1]=="*":
		cleanstring[-1]=" "
	if cleanstring[-1] != "/" and cleanstring[-1] != "@" :
		cleanFilelist.append(cleanstring)

INPUT=string.join(cleanFilelist ,',')

#get user name (for e-mail notification)
p = Popen("whoami", shell=True,stdout=PIPE,stderr=PIPE)
USERNAME = string.join(p.stdout.readlines()).rstrip("\r\n")
outerr = string.join(p.stderr.readlines() )



#get full command path
p = Popen("which %s"%(exe) , shell=True,stdout=PIPE,stderr=PIPE)
cmd_fullpath =  string.join(p.stdout.readlines())




Condor_file="""
Universe = vanilla

Executable   	= M4_EXEC
arguments       = M4_ARGUMENTS
output          = M4_OUTDIR/out 
error  		= M4_OUTDIR/error
Log             = M4_OUTDIR/log


#nice_user=True
Requirements    = ( (Arch == "X86_64")  )
Rank=mips
when_to_transfer_output = ON_EXIT
notify_user     = M4_USERNAME@ibpc.fr 
notification    = always
should_transfer_files = YES
transfer_input_files = M4_INPUT
queue 
"""

Condor_file=string.replace(Condor_file,'M4_OUTDIR', outdir)
Condor_file=string.replace(Condor_file,'M4_EXEC',cmd_fullpath)
Condor_file=string.replace(Condor_file,'M4_ARGUMENTS',string.join(arguments))
Condor_file=string.replace(Condor_file,'M4_WD',WorkingDir[:-1])
Condor_file=string.replace(Condor_file,'M4_INPUT',INPUT)
Condor_file=string.replace(Condor_file,'M4_USERNAME',USERNAME)



print "Writing condor file '.condorize.condor'"
file=open(".condorize.condor","w")
file.write(Condor_file)
file.close()


print "Standard output redirected to %s/out"%options.outputdir
print "Condor log written to ......  %s/log"%options.outputdir
print "Condor errors written to ...  %s/out"%options.outputdir

print "\nAs this is an alpha version, condor is not launched."
print "You may check .condorize.condor for errors and run manually: ' condor_submit .condorize.condor'"
#os.system("condor_submit .condorize.condor")

