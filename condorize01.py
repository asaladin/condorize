#!/usr/bin/python2.4
# condorize version 0.1
# (C)2007 Adrien Saladin
# adrien.saladin@ibpc.fr



import sys
import os
from subprocess import Popen,PIPE
import string

if (len(sys.argv) < 2):
	print """
	USAGE: condorize command_to_send_to_cluster
	"""

exe=sys.argv[1]
arguments=sys.argv[2:]



#get working directory
p = Popen("pwd", shell=True,stdout=PIPE,stderr=PIPE)
WorkingDir = string.join(p.stdout.readlines() )
outerr = string.join(p.stderr.readlines() )



#get full command path
p = Popen("which %s"%(exe) , shell=True,stdout=PIPE,stderr=PIPE)
cmd_fullpath =  string.join(p.stdout.readlines() )


#get list of all local files
p = Popen("ls -p -1" , shell=True,stdout=PIPE,stderr=PIPE)
localFileList =  p.stdout.readlines()
cleanFilelist=[]
for s in localFileList:
        cleanstring=s.rstrip("\r\n")
	if cleanstring[-1]=="*":
		cleanstring[-1]=" "
	if cleanstring[-1] != "/" and cleanstring[-1] != "@" :
		cleanFilelist.append(cleanstring)

INPUT=string.join(cleanFilelist ,',')

#get user name (for e-mail notification)
p = Popen("whoami", shell=True,stdout=PIPE,stderr=PIPE)
USERNAME = string.join(p.stdout.readlines()).rstrip("\r\n")
outerr = string.join(p.stderr.readlines() )



#get full command path
p = Popen("which %s"%(exe) , shell=True,stdout=PIPE,stderr=PIPE)
cmd_fullpath =  string.join(p.stdout.readlines())




Condor_file="""
Universe = vanilla

Executable   	= M4_EXEC
arguments       = M4_ARGUMENTS
output          = M4_WD/out 
error  		= M4_WD/error
Log             = M4_WD/log


#nice_user=True
Requirements    = ( (Arch == "INTEL")  )
Rank=mips
when_to_transfer_output = ON_EXIT
notify_user     = M4_USERNAME@ibpc.fr 
notification    = always
should_transfer_files = YES
transfer_input_files = M4_INPUT
queue 
"""

Condor_file=string.replace(Condor_file,'M4_EXEC',cmd_fullpath)
Condor_file=string.replace(Condor_file,'M4_ARGUMENTS',string.join(arguments))
Condor_file=string.replace(Condor_file,'M4_WD',WorkingDir[:-1])
Condor_file=string.replace(Condor_file,'M4_INPUT',INPUT)
Condor_file=string.replace(Condor_file,'M4_USERNAME',USERNAME)



print "Writing condor file '.condorize.condor'"
file=open(".condorize.condor","w")
file.write(Condor_file)
file.close()

os.system("condor_submit .condorize.condor")

