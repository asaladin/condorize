#!/usr/bin/env python2.4

import sys
import string
import os

class structure:
    pass


options=structure()

if sys.argv[1][0]=='-':
    if sys.argv[1][1]=='o':
        #the option is an output dir directive
        tmpoutdir = sys.argv[2]
        options.outputdir=os.path.abspath(tmpoutdir)
        os.mkdir(options.outputdir) 
        args=sys.argv[3:]
    else:
        #unknown option
        print "unregognized option"
        raise SystemExit
else:
    #first parameter is a command
    print "Repertoire de destination obligatoire !"
    raise SystemExit




recFile=args[0]
ligFile=args[1]



#determine le nombre de translations:
translat=open("translat.dat", "r")
nbtrans=0
for l in translat.readlines():
    if l[:4]=="ATOM":
        nbtrans+=1


print nbtrans, " points de depart"



Condor_file="""
####################
#
# Exemple du programme attract
#
####################

Universe = vanilla
OutDir=M4_OUTDIR


Executable      = /ibpc/rhea/saladin/Src/ptools/trunk/PyAttract/runPyattract.sh
arguments       = M4_RECEPTOR M4_LIGAND -t $(Process) --ref M4_LIGAND
output          = $(OutDir)/Attract.$(Process).out
error           = $(OutDir)/ERROR.$(Process)
Log             = $(OutDir)/LOG.$(Process)


nice_user=True
Requirements    = ( (Arch == "X86_64") &&  (Machine != "vrubel.lbt.ibpc.fr") )
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
transfer_input_files = M4_RECEPTOR,M4_LIGAND,translat.dat,attract.inp,aminon.par,rotation.dat
notify_user     = adrien.saladin@ibpc.fr
notification    = error

#queue 1
queue M4_NBTRANS
"""

Condor_file=Condor_file=string.replace(Condor_file,'M4_RECEPTOR', recFile)
Condor_file=Condor_file=string.replace(Condor_file,'M4_LIGAND', ligFile)
Condor_file=string.replace(Condor_file,'M4_OUTDIR', options.outputdir)
Condor_file=string.replace(Condor_file,'M4_NBTRANS', str(nbtrans) )


file=open("attract_ff1.condor","w")
file.write(Condor_file)
file.close()


